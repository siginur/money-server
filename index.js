const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./src/routes');

// Creating an Express app
const app = express();
const port = 4657;

// Middleware to parse JSON requests
app.use(bodyParser.json());
app.use((req, res, next) => {
	res.answer = (body) => {
		res.json({
			'success': true,
			'response': body
		})
	}
	next()
})

// Routes
app.use(routes);

// Errors handling
app.use((err, req, res, next) => {
	let errorAsString = JSON.stringify(err);
	if (errorAsString == '{}')
		errorAsString = JSON.stringify(err, Object.getOwnPropertyNames(err));
	console.error(errorAsString);
	next(err)
})
app.use((err, req, res, next) => {
	const statusCode = err.statusCode || 500;
	const message = err.message || "Unknown server error";
	const description = err['description'];
	const errorCode = err['errorCode'];
	res.status(statusCode).json({
		success: false,
		error: {
			statusCode,
			message,
			description,
			errorCode
		},
		stack: err.stack.split('\n')
	});
	next()
});

// Start the server
app.listen(port, () => {
	console.log(`Server is running on port ${port}`);
});
