const router = require('express').Router();

router.use('/altshuler-shaham', require('./altshuler-shaham.router'));
router.use('/analyst', require('./analyst.router'));
router.use('/clal', require('./clal.router'));

module.exports = router;
