const router = require('express').Router();
const altshulerShaham = require("../companies/altshuler-shaham");

router.post('/sendSMS', async (req, res, next) => {
	try {
		const passportID = req.body['passportID'];
		const phoneNumber = req.body['phoneNumber'];
		if (!passportID || !phoneNumber)
			throw new Error("Wrong request parameters")

		const result = await altshulerShaham.sendSMS(passportID, phoneNumber);

		res.answer(result);
	} catch(error) {
		next(error);
	}
});

router.post('/login', async (req, res, next) => {
	try {
		const guidID = req.body['guidID'];
		const captchaToken = req.body['captchaToken'];
		const code = req.body['code'];
		if (!guidID || !captchaToken || !code)
			throw new Error("Wrong request parameters")

		const token = await altshulerShaham.login(guidID, captchaToken, code);

		res.answer({ token });
	} catch(error) {
		next(error);
	}
});

router.get('/profile', async (req, res, next) => {
	try {
		const token = req.query['token'];
		if (!token)
			throw new Error("Wrong request parameters")

		const response = await altshulerShaham.getProfileInfo(token);
		const data = response["customerDetails"]["customerData"];
		const addresses = response["customerDetails"]["addresses"];
		res.answer({
			passportID: data["externalId"],
			firstName: data["firstName"],
			lastName: data["lastName"],
			birthdate: data["birthdate"],
			email: addresses["emailAddress"],
			phoneNumber: addresses["phone"],
			address: {
				city: addresses["city"],
				street: addresses["street"],
				house: addresses["streetNumber"],
				zip: addresses["zip"]
			}
		});
	} catch(error) {
		next(error);
	}
});

router.get('/funds/gemel', async (req, res, next) => {
	try {
		const token = req.query['token'];
		if (!token)
			throw new Error("Wrong request parameters")

		const response = await altshulerShaham.getFunds(2, token);
		let funds = [];
		response["Accounts"].forEach(account => {
			account["Funds"].forEach(fund => {
				funds.push({
					id: fund["FundNumber"],
					name: fund["FundName"],
					createdAt: fund["ReleaseDate"]
				})
			});
		});
		res.answer(funds);
	} catch(error) {
		next(error);
	}
});

router.get('/funds/hishtalmut', async (req, res, next) => {
	try {
		const token = req.query['token'];
		if (!token)
			throw new Error("Wrong request parameters")

		const response = await altshulerShaham.getFunds(4, token);
		let funds = [];
		response["Accounts"].forEach(account => {
			account["Funds"].forEach(fund => {
				funds.push({
					id: fund["FundNumber"],
					name: fund["FundName"],
					balance: fund["TotalBalance"],
					date: fund["DateOfAccuracy"],
					releaseDate: fund["ReleaseDate"],
				})
			});
		});
		res.answer(funds);
	} catch(error) {
		next(error);
	}
});

router.get('/funds/gemel/:fundID', async (req, res, next) => {
	try {
		const fundID = req.params['fundID'];
		const token = req.query['token'];
		if (!token)
			throw new Error("Wrong request parameters")

		const response = await altshulerShaham.getFundInfo(fundID, token);
		const data = response["FundFinancialData"];

		const employee = data["SumDepositEmployee"];
		const independent = null;
		const employer = data["SumDepositEmployer"];
		const compensation = data["SumDepositCompensation"];
		const total = data["TotalBalance"];

		res.answer({
			employee,
			independent,
			employer,
			compensation,
			total
		});
	} catch(error) {
		next(error);
	}
});

router.get('/funds/hishtalmut/:fundID', async (req, res, next) => {
	try {
		const fundID = req.params['fundID'];
		const token = req.query['token'];
		if (!token)
			throw new Error("Wrong request parameters")

		const response = await altshulerShaham.getFundInfo(fundID, token);
		const data = response["FundFinancialData"];

		const employee = data["SumDepositEmployee"];
		const independent = null;
		const employer = data["SumDepositEmployer"];
		const compensation = data["SumDepositCompensation"];
		const total = data["TotalBalance"];

		res.answer({
			employee,
			independent,
			employer,
			compensation,
			total
		});
	} catch(error) {
		next(error);
	}
});

router.get('/investments', async (req, res, next) => {
	try {
		const fundID = req.query['fundID'];
		const year = req.query['year'];
		const token = req.query['token'];
		if (!fundID || !year || !token)
			throw new Error("Wrong request parameters")

		const response = await altshulerShaham.getTransactions(fundID, 2, `${year}-01-01`, `${year}-12-31`, 0, token);
		res.answer({
			invested: response["FundTransactionsData"]?.["TotalSum"]["Total"] ?? null
		});
	} catch(error) {
		next(error);
	}
});

module.exports = router;
