const router = require('express').Router();
const clal = require("../companies/clal");
const cookieTools = require("../helpers/cookie-tools");

router.post('/sendSMS', async (req, res, next) => {
	try {
		const passportID = req.body['passportID'];
		const phoneNumber = req.body['phoneNumber'];
		if (!passportID || !phoneNumber)
			throw new Error("Wrong request parameters")

		const { body, statusCode } = await clal.sendSMS(passportID, phoneNumber);
		if (statusCode !== 200)
			throw new Error("Failed to send SMS");

		const token = body["Token"];

		res.answer({ token });
	} catch(error) {
	    next(error);
	}
});

router.post('/login', async (req, res, next) => {
	try {
		let token = req.body['token'];
		const code = req.body['code'];
		if (!token || !code)
			throw new Error("Wrong request parameters")

		const { body, statusCode, cookies } = await clal.login(token, code);
		if (statusCode !== 200)
			throw new Error("Failed to login");

		token = body["Token"];
		const cookiesString = cookieTools.arrayToString(cookies);

		res.answer({ token, cookies: cookiesString });
	} catch(error) {
	    next(error);
	}
});

router.get('/profile', async (req, res, next) => {
	try {
		const token = req.query['token'];
		const cookies = req.query['cookies'];
		if (!token || !cookies)
			throw new Error("Wrong request parameters")

		const response = await clal.getProfileInfo(token, cookies);
		res.answer({
			passportID: response["UserNo"],
			firstName: response["FirstName"],
			lastName: response["LastName"],
			birthdate: response["BirthDate"],
			email: response["Email"],
			phoneNumber: response["FullMobilePhone"],
			address: {
				city: response["CityName"],
				street: response["Street"],
				house: response["StreetNoAndStreetEntrence"],
				zip: response["Zip"]
			}
		});
	} catch(error) {
		next(error);
	}
});

router.get('/funds', async (req, res, next) => {
	try {
		const token = req.query['token'];
		const cookies = req.query['cookies'];
		if (!token || !cookies)
			throw new Error("Wrong request parameters")

		const response = await clal.getFunds(token, cookies);
		const gemel = response["PortfolioDataGemelHichudList"].map(fund => {
			return {
				id: fund["InsuranceNumber"],
				name: fund["InsuranceName"],
				createdAt: fund["Seniority"]
			}
		});
		const pension = response["PortfolioDataPensionFundation"].map(fund => {
			return {
				id: fund["PolicyIdEncrypted"],
				name: fund["FoundationName"],
				balance: fund["BalanceTotalSum"],
				date: fund["ValidationDate"],
				createdAt: fund["LastJoinDate"]
			}
		});

		res.answer({
			gemel,
			pension
		});
	} catch(error) {
		next(error);
	}
});

router.get('/funds/gemel/:fundID', async (req, res, next) => {
	try {
		const fundID = req.params['fundID'];
		const token = req.query['token'];
		const cookies = req.query['cookies'];
		if (!fundID, !token || !cookies)
			throw new Error("Wrong request parameters")

		const response = await clal.getGemelInfo(parseInt(fundID), token, cookies);
		const fund = response["Items"]?.find(item => item["InsuranceNumber"] === fundID);
		if (!fund)
			throw new Error("Fund not found");

		const balance = fund["Balance"];
		const date = fund["ProfitUpdateDate"];
		const invested = fund["Paths"][2]["Ammount"];

		res.answer({
			balance,
			date,
			invested
		});
	} catch(error) {
		next(error);
	}
});

router.get('/funds/pension/:fundID', async (req, res, next) => {
	try {
		const fundID = req.params['fundID'];
		const token = req.query['token'];
		const cookies = req.query['cookies'];
		if (!token || !cookies)
			throw new Error("Wrong request parameters")

		const response = await clal.getPensionInfo(fundID, token, cookies);
		const proceeds = response["Proceeds"];

		const employee = parseFloat(proceeds["Employe"].trim().replaceAll(',', ''));
		const independent = parseFloat(proceeds["Independent"].trim().replaceAll(',', ''));
		const employer = parseFloat(proceeds["Employer"].trim().replaceAll(',', ''));
		const compensation = parseFloat(proceeds["Compesation"].trim().replaceAll(',', ''));
		const total = parseFloat(proceeds["Total"].trim().replaceAll(',', ''));

		res.answer({
			employee,
			independent,
			employer,
			compensation,
			total
		});
	} catch(error) {
		next(error);
	}
});

router.get('/investments', async (req, res, next) => {
	try {
		const fundID = req.query['fundID'];
		const token = req.query['token'];
		const cookies = req.query['cookies'];
		const year = req.query['year'];
		if (!fundID, !token || !cookies)
			throw new Error("Wrong request parameters")

		let response = await clal.getFunds(token, cookies);
		const fund = response["PortfolioDataGemelHichudList"].find(item => item["InsuranceNumber"] === fundID);
		if (!fund)
			throw new Error("Fund not found");
		const txtFundationType = fund["EncryptedCashType"];
		const txtUniqueAmavid = fund["EncryptedUniqueAmavid"];

		response = await clal.getTransactions(txtFundationType, txtUniqueAmavid, token, cookies);
		const years = response["TransactionsTab"]["TransactionsList"]
			.filter(item => item["Year"] && (!year || year === item["Year"]));

		let invested = 0;
		years.forEach(item => {
			invested += parseFloat(item["Sum"]);
		})

		res.answer({
			invested
		});
	} catch(error) {
		next(error);
	}
})

module.exports = router;
