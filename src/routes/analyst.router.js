const router = require('express').Router();
const analyst = require("../companies/analyst");
const cookieTools = require("../helpers/cookie-tools");

router.post('/sendSMS', async (req, res, next) => {
	try {
		const passportID = req.body['passportID'];
		const phoneNumber = req.body['phoneNumber'];
		if (!passportID || !phoneNumber)
			throw new Error("Wrong request parameters")

		const { statusCode, cookies } = await analyst.sendSMS(passportID, phoneNumber);
		if (statusCode !== 200)
			throw new Error("Failed to send SMS");

		const cookiesString = cookieTools.arrayToString(cookies);

		res.answer({ cookies: cookiesString });
	} catch(error) {
	    next(error);
	}
});

router.post('/login', async (req, res, next) => {
	try {
		let cookiesString = req.body['cookies'];
		const code = req.body['code'];
		if (!cookiesString || !code)
			throw new Error("Wrong request parameters")

		const { statusCode, cookies } = await analyst.login(cookiesString, code);
		if (statusCode !== 200)
			throw new Error("Failed to login");

		const srcCookies = cookieTools.stringToMap(cookiesString);
		const setCookies = cookieTools.arrayToMap(cookies);
		cookiesString = cookieTools.mapToString({ ...srcCookies, ...setCookies });

		res.answer({ cookies: cookiesString });
	} catch(error) {
	    next(error);
	}
});

router.get('/balance', async (req, res, next) => {
	try {
		const fundID = req.query['fundID'];
		const cookiesString = req.query['cookies'];
		if (!fundID || !cookiesString)
			throw new Error("Wrong request parameters")

		const result = await analyst.getBalance(fundID, cookiesString);

		res.answer(result);
	} catch(error) {
	    next(error);
	}
})

module.exports = router;
