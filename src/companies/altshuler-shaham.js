const https = require("https");
const puppeteer = require("puppeteer");
const clal = require("./clal");

let browser;

async function initializeBrowser() {
	browser = await puppeteer.launch({
		headless: "new",
		args: [
			'--no-sandbox',
			'--disable-setuid-sandbox'
		]
	});
	if (!browser)
		throw new Error("Failed to initialize browser");
}

async function sendRequest(httpOptions, body) {
	return new Promise((resolve, reject) => {
		const responseEncoding = 'utf8';

		let cookies = [];
		const request = https.request(httpOptions, (res) => {
			let responseBuffs = [];
			let responseStr = '';

			res.on('data', (chunk) => {
				if (Buffer.isBuffer(chunk)) {
					responseBuffs.push(chunk);
				} else {
					responseStr = responseStr + chunk;
				}
			}).on('end', () => {
				responseStr = responseBuffs.length > 0 ?
					Buffer.concat(responseBuffs).toString(responseEncoding) : responseStr;

				// Save cookies
				const cookiesHeader = res.headers['set-cookie'];
				if (cookiesHeader) {
					cookies.push(...cookiesHeader);
				}

				try {
					if (res.statusCode == 401)
						throw new Error("Unauthorized");
					if (responseStr.startsWith("<!DOCTYPE html>") || responseStr.startsWith("<html>"))
						throw new Error("Got HTML response instead of JSON");
					if (responseStr.length === 0)
						throw new Error("Got empty response");
					const json = JSON.parse(responseStr);
					resolve({ statusCode: res.statusCode, headers: res.headers, body: json, cookies });
				} catch (e) {
					console.error("Unable to parse a string", responseStr);
					reject(e);
				}
			});
		})
			.setTimeout(0)
			.on('error', (error) => {
				reject(error);
			});

		if (body)
			request.write(JSON.stringify(body));

		request.end();
	});
}

function buildHTTPOptions(path, token) {
	const httpOptions = {
		hostname: 'online.as-invest.co.il',
		port: '443',
		path,
		method: 'POST',
		headers: {
			"Content-Type": "application/json; charset=utf-8",
			"Token": token
		}
	};
	httpOptions.headers['User-Agent'] = 'node ' + process.version;
	return httpOptions;
}

async function sendSMS(passportID, phoneNumber) {
	if (!browser)
		await initializeBrowser();

	let response;
	let page = await browser.newPage();
	try {
		page.setDefaultNavigationTimeout(60 * 1000);
		await page.goto("https://online.as-invest.co.il/login");
		let captchaToken = await page.evaluate( async () => await window["grecaptcha"]["execute"]());

		const httpOptions = {
			hostname: 'online.as-invest.co.il',
			port: '443',
			path: '/CustomersApiFront/api/Login/PreLogin',
			method: 'POST',
			headers: {
				"Content-Type": "application/json; charset=utf-8",
			}
		};
		httpOptions.headers['User-Agent'] = 'node ' + process.version;

		const { body, statusCode } = await sendRequest(httpOptions, {
			"CustomerExternalId": passportID,
			"CustomerMobilePhone": phoneNumber,
			"OTPType":1,
			"CaptchaToken":captchaToken,
			"StatementMode":false
		});
		if (statusCode !== 200)
			throw new Error("Failed to send SMS");

		const guidID = body["data"]["guidID"];
		captchaToken = await page.evaluate( async () => await window["grecaptcha"]["execute"]());

		response = {
			guidID,
			captchaToken
		}
	} catch (e) {
		throw e;
	} finally {
		page.close();
	}

	return response;
}

function login(guidID, captchaToken, code) {
	return new Promise(async (resolve, reject) => {
		try {
			const httpOptions = {
				hostname: 'online.as-invest.co.il',
				port: '443',
				path: '/CustomersApiFront/api/Login/OtpValidation',
				method: 'POST',
				headers: {
					"Content-Type": "application/json; charset=utf-8",
				}
			};
			httpOptions.headers['User-Agent'] = 'node ' + process.version;
			const { body, statusCode } = await sendRequest(httpOptions, {
				"OTP": code,
				"GuidID": guidID,
				"LawServiceAgreement": 1,
				"IsRegistered": 0,
				"CaptchaToken": captchaToken,
				"StatementMode": false
			});
			if (statusCode !== 200)
				throw new Error("Failed to login");

			const token = body["token"];
			resolve(token);
		} catch (e) {
			reject(e);
		}
	});
}

async function getProfileInfo(token) {
	return new Promise(async (resolve, reject) => {
		try {
			const httpOptions = buildHTTPOptions('/CustomersApiFront/api/General/GetCustomerGeneralData', token);
			const { body, statusCode } = await sendRequest(httpOptions, {});
			if (statusCode !== 200)
				throw new Error("Failed to get profile info");

			if (body["isError"])
				throw new Error(body["errorMessage"] ?? "Unknown error");
			resolve(body["data"]);
		} catch (e) {
			reject(e);
		}
	});
}

// Fund types: 2 - keren gemel, 4 - keren hishtalmut
async function getFunds(fundType, token) {
	return new Promise(async (resolve, reject) => {
		try {
			const httpOptions = buildHTTPOptions('/CustomersApiFront/api/Common/GetDataFromService', token);
			const { body, statusCode } = await sendRequest(httpOptions, {
				"serviceName": "GetCustomerProductFinancialData",
				"jsonParams": JSON.stringify({
					"ProductType": fundType,
					"bn_number": null,
					"Sorting": {
						"OrderColumnName": "TotalBalance",
						"OrderDirection": "desc"
					}
				})
			});
			if (statusCode !== 200)
				throw new Error("Failed to get balance");

			if (body["isError"])
				throw new Error(body["errorMessage"] ?? "Unknown error");
			const json = JSON.parse(body["data"]);
			resolve(json);
		} catch (e) {
			reject(e);
		}
	});
}

async function getFundInfo(fundID, token) {
	return new Promise(async (resolve, reject) => {
		try {
			const httpOptions = buildHTTPOptions('/CustomersApiFront/api/Common/GetDataFromService', token);
			const { body, statusCode } = await sendRequest(httpOptions, {
				"serviceName": "GetFundFinancialData",
				"jsonParams": JSON.stringify({
					"FundNumber": fundID
				})
			});
			if (statusCode !== 200)
				throw new Error("Failed to get balance");

			if (body["isError"])
				throw new Error(body["errorMessage"] ?? "Unknown error");
			const json = JSON.parse(body["data"]);
			resolve(json);
		} catch (e) {
			reject(e);
		}
	});
}

// Fund types: 2 - keren gemel, 4 - keren hishtalmut
async function getTransactions(fundID, fundType, fromDate, toDate, pageIndex, token) {
	return new Promise(async (resolve, reject) => {
		try {
			const httpOptions = buildHTTPOptions('/CustomersApiFront/api/Common/GetDataFromService', token);
			const { body, statusCode } = await sendRequest(httpOptions, {
				"serviceName": "GetFundTransactionsData",
				"jsonParams": JSON.stringify({
					"ProductType":fundType,
					"FundNumber": fundID,
					"RangeDate": {
						"FromDate": fromDate,
						"ToDate": toDate
					},
					"TransType": null,
					"ForMonth": null,
					"Paging": {
						"NumOfRows": 10,
						"PageIndex":pageIndex
					},
					"Sorting": {
						"OrderColumnName": "dateOfValue",
						"OrderDirection": "asc"
					}
				})
			});
			if (statusCode !== 200)
				throw new Error("Failed to get balance");

			if (body["isError"])
				throw new Error(body["errorMessage"] ?? "Unknown error");
			const json = JSON.parse(body["data"]);
			resolve(json);
		} catch (e) {
			reject(e);
		}
	});
}

module.exports = {
	sendSMS,
	login,
	getProfileInfo,
	getFunds,
	getFundInfo,
	getTransactions
}
