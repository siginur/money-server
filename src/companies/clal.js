const https = require("https");

async function sendRequest(httpOptions, body) {
	return new Promise((resolve, reject) => {
		const responseEncoding = 'utf8';

		let cookies = [];
		const request = https.request(httpOptions, (res) => {
			let responseBuffs = [];
			let responseStr = '';

			res.on('data', (chunk) => {
				if (Buffer.isBuffer(chunk)) {
					responseBuffs.push(chunk);
				} else {
					responseStr = responseStr + chunk;
				}
			}).on('end', () => {
				responseStr = responseBuffs.length > 0 ?
					Buffer.concat(responseBuffs).toString(responseEncoding) : responseStr;

				// Save cookies
				const cookiesHeader = res.headers['set-cookie'];
				if (cookiesHeader) {
					cookies.push(...cookiesHeader);
				}

				try {
					if (responseStr.startsWith("<!DOCTYPE html>") || responseStr.startsWith("<html>"))
						throw new Error("Got HTML response instead of JSON");
					if (responseStr.length === 0)
						throw new Error("Got empty response");
					const json = JSON.parse(responseStr);
					resolve({ statusCode: res.statusCode, headers: res.headers, body: json, cookies });
				} catch (e) {
					console.error("Unable to parse a string", responseStr);
					reject(e);
				}
			});

		})
			.setTimeout(0)
			.on('error', (error) => {
				reject(error);
			});

		if (body)
			request.write(JSON.stringify(body));

		request.end();
	});
}

function buildHTTPOptions(path, token, cookiesString, extraHeaders) {
	const httpOptions = {
		hostname: 'www.clalbit.co.il',
		port: '443',
		path,
		method: 'POST',
		headers: {
			...extraHeaders,
			"Content-Type": "application/json; charset=utf-8",
			"Cookie": cookiesString,
			"X-Xsrf-Token": token
		}
	};
	httpOptions.headers['User-Agent'] = 'node ' + process.version;
	return httpOptions;
}

function sendSMS(passportID, phoneNumber) {
	return new Promise(async (resolve, reject) => {
		try {
			const httpOptions = {
				hostname: 'www.clalbit.co.il',
				port: '443',
				path: '/umbraco/surface/LoginSurface/SMSLogin',
				method: 'POST',
				headers: {
					"Content-Type": "application/json; charset=utf-8"
				}
			};
			httpOptions.headers['User-Agent'] = 'node ' + process.version;
			const res = await sendRequest(httpOptions, {
				"FPSUserName": passportID,
				"FPSPhone": phoneNumber,
				"FPSCountryPrefix": null,
				"BoolIsAbroad": false,
				"SendOtpByVoiceCallBool": false
			});
			resolve(res);
		} catch (e) {
			reject(e);
		}
	});
}

function login(token, code) {
	return new Promise(async (resolve, reject) => {
		try {
			const httpOptions = {
				hostname: 'www.clalbit.co.il',
				port: '443',
				path: '/umbraco/surface/LoginSurface/OTPLogin',
				method: 'POST',
				headers: {
					"Content-Type": "application/json; charset=utf-8"
				}
			};
			httpOptions.headers['User-Agent'] = 'node ' + process.version;
			const res = await sendRequest(httpOptions, {
				"txtToken": token,
				"FPSOTP": code,
			});
			resolve(res);
		} catch (e) {
			reject(e);
		}
	});
}

function getProfileInfo(token, cookiesString) {
	return new Promise(async (resolve, reject) => {
		try {
			const httpOptions = buildHTTPOptions('/umbraco/surface/BaseSurface/GetCustomerData', token, cookiesString);
			const { body, cookies, statusCode } = await sendRequest(httpOptions, {});
			if (statusCode !== 200)
				throw new Error('Failed to get profile info');

			resolve(body);
		} catch (e) {
			reject(e);
		}
	});
}

function getFunds(token, cookiesString) {
	return new Promise(async (resolve, reject) => {
		try {
			const httpOptions = buildHTTPOptions('/umbraco/surface/PersonalAccountSurface/GetPortfolioHomeData', token, cookiesString, {
				"Referer": "https://www.clalbit.co.il/Portfolio"
			});
			const { body, cookies, statusCode } = await sendRequest(httpOptions, {
				txtIsExpired: false
			});
			if (statusCode !== 200)
				throw new Error('Failed to get funds');

			resolve(body);
		} catch (e) {
			reject(e);
		}
	});
}

function getGemelInfo(fundID, token, cookiesString) {
	return new Promise(async (resolve, reject) => {
		try {
			const httpOptions = buildHTTPOptions('/umbraco/surface/PersonalAccountSurface/GetPortfolioGemelDailyBalance', token, cookiesString);
			const { body, cookies, statusCode } = await sendRequest(httpOptions, {
				"objFunds": [fundID]
			});
			if (statusCode !== 200)
				throw new Error('Failed to get balance');

			resolve(body);
		} catch (e) {
			reject(e);
		}
	});
}

function getPensionInfo(fundID, token, cookiesString) {
	return new Promise(async (resolve, reject) => {
		try {
			const httpOptions = buildHTTPOptions('/umbraco/surface/PensionSurface/GetPensionPolicy', token, cookiesString);
			const { body, cookies, statusCode } = await sendRequest(httpOptions, {
				"txtPolicyId": fundID,
				"numType": "0"
			});
			if (statusCode !== 200)
				throw new Error('Failed to get balance');

			resolve(body);
		} catch (e) {
			reject(e);
		}
	});
}

function getTransactions(txtFundationType, txtUniqueAmavid, token, cookiesString) {
	return new Promise(async (resolve, reject) => {
		try {
			const httpOptions = buildHTTPOptions('/umbraco/surface/GemelSurface/GetFundationsData', token, cookiesString);
			const { body, cookies, statusCode } = await sendRequest(httpOptions, {
				txtFundationType,
				isMainInsuredBool: "true",
				txtUniqueAmavid,
				txtExpired: "false"
			});
			if (statusCode !== 200)
				throw new Error('Failed to get transactions');

			resolve(body);
		} catch (e) {
			reject(e);
		}
	});
}

module.exports = {
	sendSMS,
	login,
	getProfileInfo,
	getFunds,
	getGemelInfo,
	getPensionInfo,
	getTransactions
}
