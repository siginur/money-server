const https = require("https");
const http = require("http");
const puppeteer = require("puppeteer");

let browser;

async function initializeBrowser() {
	browser = await puppeteer.launch({
		headless: "new",
		args: [
			'--no-sandbox',
			'--disable-setuid-sandbox'
		]
	});
	if (!browser)
		throw new Error("Failed to initialize browser");
}

async function sendRequest(httpOptions, urlEncodedParams) {
	return new Promise(async (resolve, reject) => {
		const responseEncoding = 'utf8';

		let cookies = [];
		const request = https.request(httpOptions, (res) => {
			let responseBuffs = [];
			let responseStr = '';

			res.on('data', (chunk) => {
				if (Buffer.isBuffer(chunk)) {
					responseBuffs.push(chunk);
				} else {
					responseStr = responseStr + chunk;
				}
			}).on('end', () => {
				responseStr = responseBuffs.length > 0 ?
					Buffer.concat(responseBuffs).toString(responseEncoding) : responseStr;

				// Save cookies
				const cookiesHeader = res.headers['set-cookie'];
				if (cookiesHeader) {
					cookies.push(...cookiesHeader);
				}

				resolve({ statusCode: res.statusCode, headers: res.headers, body: responseStr, cookies });
			});

		})
			.setTimeout(0)
			.on('error', (error) => {
				reject(error);
			});

		if (urlEncodedParams)
			request.write(urlEncodedParams);

		request.end();
	});
}

function sendSMS(passportID, phoneNumber) {
	return new Promise(async (resolve, reject) => {
		try {
			const httpOptions = {
				hostname: 'amitim.analyst.co.il',
				port: '443',
				path: '/Modules/FinanceZone/Amit/AmitServices.ashx?action=LoginOtp',
				method: 'POST',
				headers: {
					"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
					"Accept": "application/json, text/javascript, */*; q=0.01",
					"Sec-Fetch-Site": "same-origin",
					"Accept-Language": "en-GB,en-US;q=0.9,en;q=0.8",
					"Accept-Encoding": "gzip, deflate, br",
					"Sec-Fetch-Mode": "cors",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest"
				}
			};
			httpOptions.headers['User-Agent'] = 'node ' + process.version;
			const res = await sendRequest(httpOptions, `userId=${passportID}&cellPhone=${phoneNumber}&email=&canGetSms=true&reCaptchaResponse=`);
			resolve(res);
		} catch (e) {
			reject(e);
		}
	});
}

function login(cookiesString, code) {
	return new Promise(async (resolve, reject) => {
		try {
			const httpOptions = {
				hostname: 'amitim.analyst.co.il',
				port: '443',
				path: '/Modules/FinanceZone/Amit/AmitServices.ashx?action=OtpLoginWithPassword',
				method: 'POST',
				headers: {
					"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
					"Accept": "application/json, text/javascript, */*; q=0.01",
					"Sec-Fetch-Site": "same-origin",
					"Accept-Language": "en-GB,en-US;q=0.9,en;q=0.8",
					"Accept-Encoding": "gzip, deflate, br",
					"Sec-Fetch-Mode": "cors",
					"Sec-Fetch-Dest": "empty",
					"X-Requested-With": "XMLHttpRequest",
					"Cookie": cookiesString
				}
			};
			httpOptions.headers['User-Agent'] = 'node ' + process.version;
			const res = await sendRequest(httpOptions, `password=${code}`);
			resolve(res);
		} catch (e) {
			reject(e);
		}
	});
}

async function getBalance(fundID, cookiesString) {
	if (!browser)
		await initializeBrowser();

	let response;
	let page = await browser.newPage();
	try {
		page.setDefaultNavigationTimeout(60 * 1000);
		await page.setExtraHTTPHeaders({
			'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
			'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
			'Accept-Encoding': 'gzip, deflate, br',
			'Sec-Fetch-Dest': 'document',
			'Sec-Fetch-Mode': 'navigate',
			'User-Agent': 'node ' + process.version,
			'Cookie': cookiesString,
		});

		await page.goto(`https://amitim.analyst.co.il/home/MyAccount/FundDetails.aspx?amid=${fundID}&period=1`);
		await page.waitForSelector('div#movements div.section script[type="text/javascript"]');

		const monthlyData = await page.evaluate( (varName) => window[varName], "yitrotMonthlyData" );
		const monthlyList = monthlyData['yitrotMonthlyList'];
		const lastMonth = monthlyList[monthlyList.length - 1];
		const balance = lastMonth['SUM_Yitra'];
		const date = lastMonth['Shiaruch_MounthSTR'];

		response = {
			balance,
			date
		}
	} catch (e) {
		throw e;
	} finally {
		page.close();
	}

	return response;
}

module.exports = {
	sendSMS,
	login,
	getBalance
}
