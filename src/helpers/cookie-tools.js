function arrayToMap(cookies) {
	return cookies.reduce((result, cookie) => {
		const map = stringToMap(cookie.split("; ")[0]);
		return { ...result, ...map };
	}, {});
}

function arrayToString(cookies) {
	const map = arrayToMap(cookies);
	let result = [];
	for (let key of Object.keys(map))
		result.push(`${key}=${map[key]}`);
	return result.join("; ");
}

function stringToMap(cookiesString) {
	return cookiesString.split("; ").reduce((result, cookie) => {
		const pairString = cookie.split(";")[0];
		const index = pairString.indexOf("=");
		if (index >= 0) {
			const key = pairString.substring(0, index);
			const value = pairString.substring(index + 1);
			result[key] = value;
		}
		return result;
	}, {});
}

function mapToString(cookiesMap) {
	let result = [];
	for (let key of Object.keys(cookiesMap))
		result.push(`${key}=${cookiesMap[key]}`);
	return result.join("; ");
}

module.exports = {
	arrayToMap,
	arrayToString,
	stringToMap,
	mapToString
}
